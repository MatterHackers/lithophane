﻿/*
Copyright (c) 2014, John Lewin

Based On "image to 3d printable heightmap/lithophane" by Amanda Ghassaei
http://www.instructables.com/id/3D-Printed-Photograph/
 */

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
*/

using MatterHackers.PolygonMesh;
using MatterHackers.PolygonMesh.Csg;
using MatterHackers.PolygonMesh.Processors;
using MatterHackers.VectorMath;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;

namespace MatterHackers.MatterControl.Plugins.Lithophane
{
    // Peter Blacker give some guidance to reasonable default resolution at https://reprappro.com/2013/08/08/creating-lithophanes-guest-blog/
    // RepRaps have ~0.2-0.5mm nozzles... I down-sampled my images so that each pixel is half the width of my extruder nozzle.

    public static class Lithophane
    {
        // TODO: Extract to optimized pixel data class
        // http://davidthomasbernal.com/blog/2008/03/14/c-image-processing-performance-unsafe-vs-safe-code-part-ii/
        private static byte GetBitsPerPixel(PixelFormat pixelFormat)
        {
            switch (pixelFormat)
            {
                case PixelFormat.Format24bppRgb:
                    return 24;
                    break;
                case PixelFormat.Format32bppArgb:
                case PixelFormat.Format32bppPArgb:
                case PixelFormat.Format32bppRgb:
                    return 32;
                    break;
                default:
                    throw new ArgumentException("Only 24 and 32 bit images are supported");

            }
        }

        private static byte[] GetPixels(Bitmap b)
        {
            BitmapData bData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, b.PixelFormat);

            /* GetBetsPerPixel just does a switch on the PixelFormat and returns the number */
            byte bitsPerPixel = GetBitsPerPixel(bData.PixelFormat);

            /*the size of the image in bytes */
            int size = bData.Stride * bData.Height;

            /*Allocate buffer for image*/
            byte[] data = new byte[size];

            /*This overload copies data of /size/ into /data/ from location specified (/Scan0/*/
            System.Runtime.InteropServices.Marshal.Copy(bData.Scan0, data, 0, size);

            b.UnlockBits(bData);

            return data;
        }

        class PixelInfo
        {
            public PixelInfo()
            {
            }

            public Vertex Top { get; set; }
            public Vertex Bottom { get; set; }
        }

        public static Mesh Generate(Bitmap image, int onPlateWidth, double maxZ, double nozzleWidth, double pixelsPerMM, bool invert, BackgroundWorker backgroundWorker)
        {
            var pixelWidth = pixelsPerMM * onPlateWidth;

            var resizedImage = ToResizedGrayscale(image as Bitmap, InterpolationMode.Default, (int) pixelWidth);

            // TODO: Move this to a user supplied value
            double baseThickness = nozzleWidth;     // base thickness (in mm)
            double zRange = maxZ - baseThickness;
          
            // Dimensions of image
            var width = resizedImage.Width;
            var height = resizedImage.Height;

            var zScale = zRange / 255;

            var pixelData = GetPixels(resizedImage);

            Stopwatch stopwatch = Stopwatch.StartNew();

            var mesh = new Mesh();

            var rescale = (double) onPlateWidth / resizedImage.Width;;

            // Build an array of PixelInfo objects from each pixel
            // Collapse from 4 bytes per pixel to one - makes subsequent processing more logical and has minimal cost
            var pixels = pixelData.Where((x, i) => i % 4 == 0)

                // Interpolate the pixel color to zheight
                .Select(b => baseThickness + (invert ? 255 - b : b) * zScale)

                // Create a Vector3 for each pixel at the computed x/y/z
                .Select((z, i) => mesh.CreateVertex(new Vector3(
                    i % width * rescale,
                    (i - i % width) / width * rescale*-1,
                    z),
                    true,
                    true))

                // Create a mirrored vector for the pixel at z0 and return with top/bottom paired together
                .Select(vec => new PixelInfo(){
                    Top = vec,
                    Bottom = mesh.CreateVertex(new Vector3(
                        vec.Position.x,
                        vec.Position.y,
                        0),
                        true,
                        true)
                }).ToArray();

            Console.WriteLine("ElapsedTime - PixelInfo Linq Generation: {0}", stopwatch.ElapsedMilliseconds);
            stopwatch.Restart();

            // Select pixels along image edges
            var backRow = pixels.Take(width).Reverse().ToArray();
            var frontRow = pixels.Skip((height-1) * width).Take(width).ToArray();
            var leftRow = pixels.Where((x, i) => i % width == 0).ToArray();
            var rightRow = pixels.Where((x, i) => (i + 1) % width == 0).Reverse().ToArray();

            int k,
                nextJ,
                nextK;

            var notificationInterval = 100;

            var workCount = (resizedImage.Width - 1) * (resizedImage.Height - 1) +
                            (height - 1) +
                            (width - 1);

            var workIndex = 0;

            // Vertical faces: process each row and column, creating the top and bottom faces as appropriate
            for (int i = 0; i < resizedImage.Height-1; ++i)
            {
                var startAt = i * width;

                // Process each column
                 for (int j = startAt; j < startAt + resizedImage.Width - 1; ++j)
                {
                    k = j + 1;
                    nextJ = j + resizedImage.Width;
                    nextK = nextJ + 1;

                    // Create north, then south face
                    mesh.CreateFace(new Vertex[] { pixels[k].Top, pixels[j].Top, pixels[nextJ].Top, pixels[nextK].Top });
                    mesh.CreateFace(new Vertex[] { pixels[j].Bottom, pixels[k].Bottom, pixels[nextK].Bottom, pixels[nextJ].Bottom });
                    workIndex++;

                    if (workIndex % notificationInterval == 0)
                    {
                        backgroundWorker.ReportProgress((int)((float)workIndex / workCount * 100));
                    }
                }
            }

            // Side faces: East/West
            for (int j = 0; j < height - 1; ++j)
            {
                //Next row
                k = j + 1;

                // Create east, then west face
                mesh.CreateFace(new Vertex[] { leftRow[k].Top, leftRow[j].Top, leftRow[j].Bottom, leftRow[k].Bottom });
                mesh.CreateFace(new Vertex[] { rightRow[k].Top, rightRow[j].Top, rightRow[j].Bottom, rightRow[k].Bottom });
                workIndex++;

                if (workIndex % notificationInterval == 0)
                {
                    backgroundWorker.ReportProgress(workIndex / workCount * 100);
                }
            }

            // Side faces: North/South
            for (int j = 0; j < width - 1; ++j)
            {
                // Next row
                k = j + 1;

                // Create north, then south face
                mesh.CreateFace(new Vertex[] { frontRow[k].Top, frontRow[j].Top, frontRow[j].Bottom, frontRow[k].Bottom });
                mesh.CreateFace(new Vertex[] { backRow[k].Top, backRow[j].Top, backRow[j].Bottom, backRow[k].Bottom });
                workIndex++;

                if (workIndex % notificationInterval == 0)
                {
                    backgroundWorker.ReportProgress(workIndex / workCount * 100);
                }
            }

            Console.WriteLine("ElapsedTime - Face Generation: {0}", stopwatch.ElapsedMilliseconds);

            resizedImage.Dispose();

            return mesh;
        }

        // http://tech.pro/tutorial/660/csharp-tutorial-convert-a-color-image-to-grayscale
        public static Bitmap ToResizedGrayscale(Bitmap original, InterpolationMode interpolationMode = InterpolationMode.Bilinear, int width = 0)
        {
            Bitmap newBitmap;

            if (width > 0)
            {
                var scale = (double)width / original.Width;
                var resize = new Size(width, (int)(original.Height * scale));

                //create a blank bitmap the same size as original
                newBitmap = new Bitmap(resize.Width, resize.Height);
            }
            else
            {
                newBitmap = new Bitmap(original.Width, original.Height);
            }

            //get a graphics object from the new image
            using (var g = Graphics.FromImage(newBitmap))
            {
                if (interpolationMode != InterpolationMode.Bilinear)
                {
                    g.InterpolationMode = interpolationMode;
                }

                // The default behavior of HighQualityBicubic produces semi-transparent outlines at the borders. This,
                // along with the WrapMode change below help mitigate the issue
                if (interpolationMode == InterpolationMode.HighQualityBicubic)
                {
                    // add below line
                    g.CompositingMode = CompositingMode.SourceCopy;
                }

                //create the grayscale ColorMatrix
                ColorMatrix colorMatrix = new ColorMatrix(
                    new float[][] 
                    {
                        new float[] {.3f, .3f, .3f, 0, 0},
                        new float[] {.59f, .59f, .59f, 0, 0},
                        new float[] {.11f, .11f, .11f, 0, 0},
                        new float[] {0, 0, 0, 1, 0},
                        new float[] {0, 0, 0, 0, 1}
                    });

                //create some image attributes
                ImageAttributes attributes = new ImageAttributes();

                //set the color matrix attribute
                attributes.SetColorMatrix(colorMatrix);

                // Resolve the issue caused with HighQualityBicubic interpolation due to averaging transparent pixels beyond the image boundaries
                // http://stackoverflow.com/questions/1890605/ghost-borders-ringing-when-resizing-in-gdi
                if (interpolationMode == InterpolationMode.HighQualityBicubic)
                {
                    attributes.SetWrapMode(WrapMode.TileFlipXY);
                }

                //draw the original image on the new image
                //using the grayscale color matrix
                g.DrawImage(original, new Rectangle(0, 0, newBitmap.Width, newBitmap.Height),
                     0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);
            }

            return newBitmap;
        }
    }
}
