﻿/*
Copyright (c) 2014, John Lewin
Copyright (c) 2014, Lars Brubaker
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

using System;
using System.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using MatterHackers.Agg.UI;
using MatterHackers.MatterControl.PluginSystem;
using MatterHackers.MatterControl.DataStorage;
using MatterHackers.MatterControl;
using MatterHackers.MatterControl.VersionManagement;
using MatterHackers.MatterControl.CreatorPlugins;

namespace MatterHackers.MatterControl.Plugins.Lithophane
{
    public class LithophanePlugin : MatterControlPlugin
    {
        GuiWidget mainApplication;
        
        public LithophanePlugin()
        { 
        }

        public override void Initialize(GuiWidget application)
        {
            // Register a Design Tool/Add-on creator
            RegisteredCreators.Instance.RegisterLaunchFunction(new CreatorInformation(
                (object sender, EventArgs e) => new LithophaneMainWindow(),
                "259.png", 
                "Lithophane Creator"));
            
            // Store a reference to the main application
            mainApplication = application;
        }

        public override string GetPluginInfoJSon()
        {
return @"
{
    ""Name"": ""Lithophane Creator"",
    ""UUID"": ""B07B4EB0-CAFD-4721-A04A-FD9C3E001D2B"",
    ""About"": ""A Lithophane Creator."",
    ""Developer"": ""John Lewin"",
    ""URL"": ""https://hifov.com""
}";
        }
    }
}
